#!/usr/bin/python
from flask import Flask
app = Flask(__name__)

@app.route('/', defaults={'path': ''})
@app.route('/<path:path>')
def function1(path):
    res = "i am function 1: %s" % path
    print res
    return res

if __name__ == '__main__':
    app.run(host='0.0.0.0')

